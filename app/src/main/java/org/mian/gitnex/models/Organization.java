package org.mian.gitnex.models;

/**
 * Author M M Arif
 */

public class Organization {

    private int id;
    private String avatar_url;
    private String description;
    private String full_name;
    private String location;
    private String username;
    private String website;

    public int getId() {
        return id;
    }

    public String getAvatar_url() {
        return avatar_url;
    }

    public String getDescription() {
        return description;
    }

    public String getFull_name() {
        return full_name;
    }

    public String getLocation() {
        return location;
    }

    public String getUsername() {
        return username;
    }

    public String getWebsite() {
        return website;
    }

}
